<?php
namespace Drupal\switips_commerce\Service;

use Drupal\switips_commerce\Switips;

class SwitipsService {	

  public function sendOrder($args, $api_url = '', $currencies = null, $states = null){

    $switips = new Switips($api_url, $currencies = null, $states = null);
    $json = $switips->sendRequestPost($args);

    return $json;      
  }
  
  public function getDefaultDomain($api_url = ''){

    $switips = new Switips('empty');
    $api_url = $switips->getDefaultDomain($api_url);

    return $api_url;
  }
  
  public function saveUid($order_id, $uid){
    $insert = \Drupal::database()->merge('switips_commerce');
    $insert->insertFields(array(
      'order_id' => $order_id,
      'uid' => $uid,
      ));
    $insert->updateFields(array(
      'uid' => $uid,
      ));
    $insert->condition('order_id', $order_id);
    $insert->execute();	      
  }
  
  public function getUid($order_id){
    $q = \Drupal::database()->select('switips_commerce', 'f1');
    $q->fields('f1', ['uid']);
    $q->condition('f1.order_id', $order_id);
    $output = $q->execute()->fetchAssoc();

    return $output['uid'];       
  }  
  
  public function deleteUid($order_id){
    $del = \Drupal::database()->delete('switips_commerce');
    $del->condition('order_id', $order_id);
    $del->execute();       
  }   
}