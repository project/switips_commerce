<?php
namespace Drupal\switips_commerce\EventSubscriber;

use Drupal\commerce_payment\Event\PaymentEvents;
use Drupal\commerce_payment\Event\PaymentEvent;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderEvents;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\Entity\EntityTypeManager;

class SwitipsOrderSubscriber implements EventSubscriberInterface {

  public static function getSubscribedEvents() {
    $events = [
      //'commerce_order.place.post_transition' => 'sendSwitipsNewOrder',
      OrderEvents::ORDER_UPDATE => 'sendSwitipsUpdateOrder',
      OrderEvents::ORDER_DELETE => 'sendSwitipsDeleteOrder',
      PaymentEvents::PAYMENT_PRESAVE => 'sendSwitipsOrderPaymentPreSave',
    ];
    return $events;
  }  

  /*
   * Новый заказ
  */
/*
  public function sendSwitipsNewOrder(WorkflowTransitionEvent $event) {
    $switips = unserialize(\Drupal::state()->get('switips_settings'));
    
    if($switips['status_active'] == 0){
      return;
    }
    
    $cookies = \Drupal::request()->cookies;
    $uid = $cookies->get('Drupal_visitor_uid', 'no');    
    
    if($uid != 'no'){
      // @var \Drupal\commerce_order\Entity\OrderInterface $order 
      $order = $event->getEntity();    
      $items = $order->getItems();
      
      $order_number = $order->getOrderNumber();
      $total_price = $order->getTotalPrice();
      $created_time = $order->getCreatedTime();

      $args = [      
        'secret_key' => $switips['secret_key'],
        'merchant_id' => $switips['merchant_id'],
        'campaign_id' => 0,
        'category_id' => 0,
        'user_id' => $uid,    
        'transaction_id' => $order_number,
        'transaction_amount' => $total_price->getNumber(),
        'transaction_amount_currency' => $total_price->getNumber(),
        'currency' => $total_price->getCurrencyCode(),      
        'tt_date' => $created_time,
        'stat' => 'new', 
        'commission_amount' => $this->getCommissionAmount($items, $switips['catalog']),
      ];    
      
      $json = \Drupal::service('switips_commerce.switips_service')->sendOrder($args, $switips['api_url']);
      \Drupal::service('switips_commerce.switips_service')->saveUid($order_number, $uid);

      if($json->status == 'ok'){
        \Drupal::logger('switips_commerce')
          ->info(t('Заказ № @order_id отправлен в Switips.', ['@order_id' => $order_number]));      
      }else{
        \Drupal::logger('switips_commerce')
          ->info(t('Заказ № @order_id не отправлен в Switips.', ['@order_id' => $order_number])); 
      }
    }else{
      \Drupal::logger('switips_commerce')
        ->info(t('Заказ № @order_id не отправлен в Switips. Пустой UID', ['order_id' => $order_number]));
    }
  } 
*/  

  /*
   * Обновление статуса при платежах
  */
  public function sendSwitipsOrderPaymentPreSave(PaymentEvent $event) {
    $switips = unserialize(\Drupal::state()->get('switips_settings'));

    if($switips['status_active'] == 0){
      return;
    }

    $payment = $event->getPayment();
    $order = $payment->getOrder();
    $order_number = $order->getOrderNumber();

    /*
     * При создании платежа в админке, вначале выствляется статус:
     * pending - в ожидании. Его тут не используем
    */
    $order_state = $payment->get('state')->value;

    $state = '';
    switch($order_state) {
      case 'refunded':
        $state = 'canceled';
        break;
      case 'completed':
        $state = 'confirmed';
        break;
    }

    if(is_numeric($order_number) && $state != ''){      
      $uid = \Drupal::service('switips_commerce.switips_service')->getUid($order_number);

      if(is_null($uid)){
        return;
      } 

      $total_price = $order->getTotalPrice();
      $created_time = $order->getCreatedTime();
      $items = $order->getItems();

      $args = [      
        'secret_key' => $switips['secret_key'],
        'merchant_id' => $switips['merchant_id'],
        'campaign_id' => 0,
        'category_id' => 0,
        'user_id' => $uid,    
        'transaction_id' => $order_number,
        'transaction_amount' => $total_price->getNumber(),
        'transaction_amount_currency' => $total_price->getNumber(),
        'currency' => $total_price->getCurrencyCode(),      
        'tt_date' => $created_time,
        'stat' => $state, 
        'commission_amount' => $this->getCommissionAmount($items, $switips['catalog']),
      ]; 

      $json = \Drupal::service('switips_commerce.switips_service')->sendOrder($args, $switips['api_url']);

      if($json->status == 'ok'){
        \Drupal::logger('switips_commerce')
          ->info(t('Order # @order_id updated in Switips.', ['@order_id' => $order_number]));      
      }else{
        \Drupal::logger('switips_commerce')
          ->info(t('Order # @order_id is not updated in Switips', ['@order_id' => $order_number])); 
      }

    } 
    
  }


  /*
   * Создание/Обновление заказа
  */
  public function sendSwitipsUpdateOrder(OrderEvent $event) {
    $switips = unserialize(\Drupal::state()->get('switips_settings'));
    $route_name = \Drupal::routeMatch()->getRouteName();
    
    if($switips['status_active'] == 0){
      return;
    }
    
    if($route_name == 'entity.commerce_payment.operation_form'){
      return;
    }        

    $order = $event->getOrder();   
    $order_number = $order->getOrderNumber();
    $order_state = $order->get('state')->value;

    $state = 'new';
    switch($order_state) {
      case 'draft':
        $state = '';
        break;       
    }    
    
    if(is_numeric($order_number) && $state != ''){     

      $cookies = \Drupal::request()->cookies;
      $uid = $cookies->get('Drupal_visitor_uid', 'no');    
      
      if($uid == 'no'){
        $uid = \Drupal::service('switips_commerce.switips_service')->getUid($order_number);
      } 
      
      if(is_null($uid)){
        return;
      }

      $total_price = $order->getTotalPrice();
      $created_time = $order->getCreatedTime();
      $items = $order->getItems();

      if($order->isPaid()){
        $state = 'paid';
      }

      $args = [      
        'secret_key' => $switips['secret_key'],
        'merchant_id' => $switips['merchant_id'],
        'campaign_id' => 0,
        'category_id' => 0,
        'user_id' => $uid,    
        'transaction_id' => $order_number,
        'transaction_amount' => $total_price->getNumber(),
        'transaction_amount_currency' => $total_price->getNumber(),
        'currency' => $total_price->getCurrencyCode(),      
        'tt_date' => $created_time,
        'stat' => $state, 
        'commission_amount' => $this->getCommissionAmount($items, $switips['catalog']),
      ];

      $json = \Drupal::service('switips_commerce.switips_service')->sendOrder($args, $switips['api_url']);

      if($json->status == 'ok'){
        \Drupal::logger('switips_commerce')
          ->info(t('Order # @order_id updated in Switips.', ['@order_id' => $order_number]));
        \Drupal::service('switips_commerce.switips_service')->saveUid($order_number, $uid);
      }else{
        \Drupal::logger('switips_commerce')
          ->info(t('Order # @order_id is not updated in Switips', ['@order_id' => $order_number])); 
      }

    }  
  }  
  
  /*
   * Удаление заказа
  */
  public function sendSwitipsDeleteOrder(OrderEvent $event) {
    $switips = unserialize(\Drupal::state()->get('switips_settings'));
    
    if($switips['status_active'] == 0){
      return;
    }

    $order = $event->getOrder();   
    $order_number = $order->getOrderNumber();  
    $total_price = $order->getTotalPrice();
    $created_time = $order->getCreatedTime();
    $items = $order->getItems();
    $uid = \Drupal::service('switips_commerce.switips_service')->getUid($order_number);

    $args = [      
      'secret_key' => $switips['secret_key'],
      'merchant_id' => $switips['merchant_id'],
      'campaign_id' => 0,
      'category_id' => 0,
      'user_id' => $uid,    
      'transaction_id' => $order_number,
      'transaction_amount' => $total_price->getNumber(),
      'transaction_amount_currency' => $total_price->getNumber(),
      'currency' => $total_price->getCurrencyCode(),      
      'tt_date' => $created_time,
      'stat' => 'canceled', 
      'commission_amount' => $this->getCommissionAmount($items, $switips['catalog']),
    ];  
    
    if(!is_null($uid)){    
      $json = \Drupal::service('switips_commerce.switips_service')->sendOrder($args, $switips['api_url']);       
    }

    if(isset($json->status) && $json->status == 'ok'){
      $uid = \Drupal::service('switips_commerce.switips_service')->deleteUid($order_number);
      \Drupal::logger('switips_commerce')
        ->info(t('Order # @order_id canceled in Switips', ['@order_id' => $order_number]));      
    }else{
      \Drupal::logger('switips_commerce')
        ->info(t('Order # @order_id not canceled in Switips', ['@order_id' => $order_number])); 
    }  
  }    
  
  public function getCommissionAmount($items, $field_name){

    $output = 0;
    if($field_name == 'empty'){
      return $output;
    }

    foreach($items as $item){
      /* Drupal\commerce_order\Entity\OrderItemInterface */
      $entity = $item->getPurchasedEntity();
      $qty = $item->getQuantity();
      $unit_price = $item->getUnitPrice(); // Цена одной единицы товара
      $fields = $entity->getFields();  

      $product_id = $fields['product_id']->getValue()[0]['target_id'];
      $product = \Drupal::entityManager()->getStorage('commerce_product')->load($product_id);      
      $product_commission = $this->checCommission($product->field_switips_commission_p->value);      

      if($product_commission > 0){
        
        if(strpos($product_commission, '%') !== false){
          $product_commission = str_replace('%', '', $product_commission);
          $product_commission = ($unit_price->getNumber()/100)*$product_commission;          
          $output += ($qty*$product_commission);
        }else{
          $output += ($qty*$product_commission);
        }

        continue;
      }

      $tids = $product->get($field_name)->getValue();

      $catalog = array_pop($tids);
      if(isset($catalog['target_id'])){
        $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($catalog['target_id']);
        
        $catalog_commission = $this->checCommission($term->field_switips_commission_t->value);
        
        if($catalog_commission > 0){
          
          if(strpos($catalog_commission, '%') !== false){
            $catalog_commission = str_replace('%', '', $catalog_commission);
            $catalog_commission = ($unit_price->getNumber()/100)*$catalog_commission;          
            $output += ($qty*$catalog_commission);
          }else{
            $output += ($qty*$catalog_commission);
          } 
          continue;
        }
      }      
    }
    
    return $output;
  }
  
  public function checCommission($value = 0){
    $pattern = '/[^0-9 %]+/msiu';
    if(preg_match($pattern, $value)){      
      return 0; // Найдены запрещенные символы
    }else{
      return str_replace(' ', '', $value);
    }  
  }
  
}