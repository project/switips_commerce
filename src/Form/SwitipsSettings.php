<?php
namespace Drupal\switips_commerce\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;
use Drupal\switips_commerce\Switips;

class SwitipsSettings extends FormBase {

	public function getFormId() {
		return 'switips_settings_form';
	}

	public function buildForm(array $form, FormStateInterface $form_state) {

    $commission_description = $this->t('To calculate the agency fee, you need to add the appropriate fields in the «product categories» and in the «product». <br>');
    $commission_description .= $this->t('Indicate which taxonomy dictionary contains product categories. Also click on the checkbox «Add a field with an agent fee to the product»'); 
    
    $switips = unserialize(\Drupal::state()->get('switips_settings'));
    $vocabulary = \Drupal\taxonomy\Entity\Vocabulary::loadMultiple();

    if(isset($switips['api_url'])){
      $switips['api_url'] = \Drupal::service('switips_commerce.switips_service')->getDefaultDomain($switips['api_url']);
    }else{
      $switips['api_url'] = \Drupal::service('switips_commerce.switips_service')->getDefaultDomain();
    }
    
    \Drupal::state()->set('switips_settings', serialize($switips));

    $form['status_active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable module'),
      '#default_value' => isset($switips['status_active']) ? $switips['status_active'] : '',        
    ]; 

    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL for requests to switips'),
      '#default_value' => isset($switips['api_url']) ? $switips['api_url'] : '',
    ];
    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => isset($switips['secret_key']) ? $switips['secret_key'] : '',
      '#description' => $this->t('(secret_key) Available in your office.switips.com personal account in the Settings - For developer'),
    ];
    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Partner ID'),
      '#default_value' => isset($switips['merchant_id']) ? $switips['merchant_id'] : '',
      '#description' => $this->t('(merchant_id) Assigned by the company and remains static'),
    ];
    $default_catalog = isset($switips['catalog']) ? $switips['catalog'] : '';
    $form['catalog'] = [
      '#type' => 'select',
      '#title' => $this->t('Product Catalog'),
      '#options' => $this->getFieldList(),
      '#default_value' => [$default_catalog],
      '#description' => $this->t('Select the field responsible for the catalog in the product'),
    ];      
    
    $default_vocabulary = isset($switips['vocabulary']) ? $switips['vocabulary'] : '';
    $form['commission'] = array(
      '#type' => 'details',
      '#title' => $this->t('Agent s commission'),
      '#open' => false,
      '#description' => $this->t($commission_description),
    );
      $form['commission']['vocabulary'] = [
        '#type' => 'select',
        '#options' => $this->getOptions($vocabulary),
        '#default_value' => [$default_vocabulary],
      ];  
      $form['commission']['product_add_field'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Add an agent fee field to a product'),
        '#default_value' => isset($switips['product_add_field']) ? $switips['product_add_field'] : '',        
      ];      
    
    $form['submit'] = [
      '#type' => 'submit',
      '#name' => 'submit',
      '#value' => $this->t('Save'),
	  ];

	return $form;
	}

	public function submitForm(array &$form, FormStateInterface $form_state) {
    
    $data = $form_state->getValues();
		\Drupal::state()->set('switips_settings', serialize($data));

    $field_switips_commission_t = FieldStorageConfig::loadByName('taxonomy_term', 'field_switips_commission_t');
    if(is_null($field_switips_commission_t) && $data['vocabulary'] != 'empty'){
      $this->addCommissionFieldsTaxonomy($data['vocabulary']);
    }
    
    $field_switips_commission_p = FieldStorageConfig::loadByName('commerce_product', 'field_switips_commission_p');
    if(is_null($field_switips_commission_p) && $data['product_add_field'] > 0){
      $this->addCommissionFieldsProduct('default');
    }
	}

  public function getOptions($vocabularies){
    $output = ['empty'=>$this->t('Not chosen')];
    foreach($vocabularies as $vid=>$vocabulary){
      $output[$vid] = $vocabulary->get('name');
    }
    
    return $output;
  }
  
  public function getFieldList(){
    $fields = \Drupal::service('entity_field.manager')
                  ->getFieldDefinitions('commerce_product', 'default');
                  
    $output = ['empty'=>$this->t('Not chosen')];
    foreach($fields as $key=>$value){
      $label = $value->getLabel();
      
      if(is_string($label)){
        $output[$key] = $label;
      }else{
        $output[$key] = $label->render();
      }
    } 
    
    return $output;
  }
  
  public function addCommissionFieldsTaxonomy($bundle){
    
    $entity_type = 'taxonomy_term';
    $field_name = 'field_switips_commission_t';

    FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'langcode' => 'en',
      'type' => 'string',
      'status' => 1,
      'locked' => 0,
      'cardinality' => 1,
      'settings' => [
        'max_length' => 40,
        'is_ascii' => 0,
        'case_sensitive' => 0,
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'required' => 0,
      'label' => $this->t('Agent s commission'),
      'description' => $this->t('The amount of the agency fee is transferred to Switips. <br> If you indicate the% sign, the amount of remuneration will be calculated as% of the value of the goods <br>Example: <b> 10% </b>'),
      'default_value' => [
        'value' => 0,
      ]
    ])->save(); 

    $form_display = \Drupal::service('entity_display.repository')->getFormDisplay($entity_type, $bundle);
    $form_display->setComponent($field_name,[
      'id' => 'string_textfield',
      'weight' => 0,
    ]);
    $form_display->setStatus(true);
    $form_display->save();
  
    $view_display = \Drupal::service('entity_display.repository')->getViewDisplay($entity_type, $bundle);
    $view_display->removeComponent($field_name);
    $view_display->save();    
  }
  
  public function addCommissionFieldsProduct($bundle){
    
    $entity_type = 'commerce_product';
    $field_name = 'field_switips_commission_p';

    FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'langcode' => 'en',
      'type' => 'string',
      'status' => 1,
      'locked' => 0,
      'cardinality' => 1,
      'settings' => [
        'max_length' => 40,
        'is_ascii' => 0,
        'case_sensitive' => 0,
      ],
    ])->save();

    FieldConfig::create([
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'required' => 0,
      'label' => $this->t('Agent s commission'),
      'description' => $this->t('The amount of the agency fee is transferred to Switips. <br> If you indicate the% sign, the amount of remuneration will be calculated as% of the value of the goods <br>Example: <b> 10% </b>'),
      'default_value' => [
        'value' => 0,
      ]
    ])->save(); 

    $form_display = \Drupal::service('entity_display.repository')->getFormDisplay($entity_type, $bundle);
    $form_display->setComponent($field_name,[
      'id' => 'string_textfield',
      'weight' => 2,
    ]);
    $form_display->setStatus(true);
    $form_display->save();
  
    $view_display = \Drupal::service('entity_display.repository')->getViewDisplay($entity_type, $bundle);
    $view_display->removeComponent($field_name);
    $view_display->save();    
  }
  
}
